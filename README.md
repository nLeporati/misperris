Entrega parte 3: rama Master

Los requerimientos para su uso son:\
* Django==2.1.2\
* django-progressive-web-app==0.1.1\
* djangorestframework==3.9.0\
* Pillow==5.3.0\
* python-social-auth==0.3.6\
* pytz==2018.5\
* social-auth-app-django==3.1.0\


Ya existe un usuario administrador creado, sus datos son:\
* username: admin\
* password: admin\
* email: paramisperris@gmail.com

Existe un usuario comun creado:\
* username: demo\
*  password: duoc2018\
*  email: paramisperris@gmail.com

Si se esta en modo PWA Offline y quiere realizar una accion con la db refrescar la pagina nuevamente