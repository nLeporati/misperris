from apps.rescatados.models import Perro
from rest_framework import serializers


class PerroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Perro
        fields = ('fotografia', 'nombre', 'razaPredominante', 'descripcion', 'estado')