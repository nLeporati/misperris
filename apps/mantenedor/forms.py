# from django.contrib.auth.models import User
from .models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.validators import RegexValidator
from .validators import validar_letras, validar_fecha
from django import forms

REGIONES = (
    ('arica','Región de Arica y Parinacota'),
	('tarapaca','Región de Tarapaca'),
	('antofagasta','Región de Antofagasta'),
	('atacama','Región de Atacama'),
	('coquimbo','Región de Coquimbo'),	
	('valparaiso','Región de Valparaiso'),
	('ohiggins','Región del Libertador General Bdo. OHiggins'),
	('maule','Región del Maule'),
	('biobio','Región de Biobio'),
	('araucania','Región de La Araucania'),
	('rios','Región de Los Rios'),
	('lagos','Región de Los Lagos'),
	('aisen','Región Aisén del Gral. Carlos Ibáñez del Campo'),
	('magallanes','Región de Magallanes y de la Antártica Chilena'),
	('metropolitana','Región Metropolitana de Santiago'),
)

class RegistroForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(RegistroForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['first_name'].validators = [validar_letras]
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['region'].required = True
        self.fields['comuna'].required = True
        self.fields['rut'].required = True
        self.fields['numeroTelefono'].required = True
        self.fields['fechaNacimiento'].required = True
        self.fields['fechaNacimiento'].validators = [validar_fecha]
        self.fields['tipoVivienda'].required = True
        self.fields['password1'].widget = forms.TextInput(attrs={'class':'form-control','type':'password'})
        self.fields['password2'].widget = forms.TextInput(attrs={'class':'form-control','type':'password'})        

    class Meta:
        model = User
        fields = (
            'username',
            'rut',
            'first_name',
            'last_name',
            'last_name',
            'fechaNacimiento',
            'email',
            'numeroTelefono',            
            'region',
            'comuna',            
            'tipoVivienda',
            'password1',
            'password2'                                 
        )
        widgets = {
            'rut': forms.TextInput(attrs={'class':'form-control','onkeypress':'return validarRut(event)'}),
            'first_name': forms.TextInput(attrs={'class':'form-control','onkeypress':'return validarLetras(event)'}),
            'last_name': forms.TextInput(attrs={'class':'form-control','onkeypress':'return validarLetras(event)'}),
            'fechaNacimiento': forms.DateInput(attrs={'class':'form-control','type': 'date','max': '31-01-2018'}),
            'numeroTelefono': forms.TextInput(attrs={'class':'form-control','onkeypress':'return validarNumeros(event)'}),
            'region': forms.Select(choices=REGIONES ,attrs={'id':'id_cmbRegiones','class':'custom-select'}),            
            'comuna': forms.Select(attrs={'id':'id_cmbComunas','class':'custom-select'}),
            'tipoVivienda': forms.Select(attrs={'class':'custom-select'}),    
            'username': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control','type':'email'})
        }
        labels = {
            'numeroTelefono': 'Numero de teléfono',
            'tipoVivienda': 'Tipo de vivienda',
            'fechaNacimiento': 'Fecha de nacimiento',
        }

