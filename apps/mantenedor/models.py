from django.db import models
# from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from .validators import validar_rut
# Create your models here.

VIVIENDA_TIPOS = (
    ('Casa con Patio Grande', 'Casa con Patio Grande'),
    ('Casa con Patio Pequeño', 'Casa con Patio Pequeño'),
    ('Casa sin Patio', 'Casa sin Patio'),
    ('Departamento', 'Departamento'),
)

class User(AbstractUser):
    rut = models.CharField(max_length=15, validators=[validar_rut], blank=True, null=True)
    fechaNacimiento = models.DateField(blank=True, null=True)
    region = models.CharField(max_length=80, blank=True, null=True)
    comuna = models.CharField(max_length=50, blank=True, null=True)
    tipoVivienda = models.CharField(max_length=25, blank=True, null=True, choices=VIVIENDA_TIPOS)
    numeroTelefono = models.IntegerField(blank=True, null=True)

