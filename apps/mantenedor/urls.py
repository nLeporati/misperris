from django.urls import path
from django.contrib.auth.views import LoginView, logout_then_login
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.auth.views import logout_then_login, PasswordResetForm, PasswordResetView, PasswordResetConfirmView, PasswordResetCompleteView, PasswordResetDoneView

from .views import index, RegistroUsuario, logout

# app_name = 'mantenedor'

urlpatterns = [
    path('', index, name="index"),
    path('registro/', RegistroUsuario.as_view(), name="registro"),
    path('login/', LoginView.as_view(template_name='mantenedor/login.html'), name="login"),
    path('logout/', logout_then_login, {'login_url': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    # path('logout/', logout, name='logout'),

    path('password-reset', PasswordResetView.as_view(template_name='pswd-reset/password_reset_form.html', email_template_name='pswd-reset/password_reset_email.html'), name="password_reset"),
    path('password-reset/done', PasswordResetDoneView.as_view(template_name='pswd-reset/password_reset_done.html'), name="password_reset_done"),
    path('password-reset/confirm/<uidb64>/<token>', PasswordResetConfirmView.as_view(template_name='pswd-reset/password_reset_confirm.html'), name="password_reset_confirm"),
    path('password-reset/complete', PasswordResetCompleteView.as_view(template_name='pswd-reset/password_reset_complete.html'), name="password_reset_complete")
]