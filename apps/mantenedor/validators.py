import re
from itertools import cycle
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
 
def validar_rut(rut):
	rut = rut.upper()
	rut = rut.replace("-","")
	rut = rut.replace(".","")
	aux = rut[:-1]
	dv = rut[-1:]
 
	revertido = map(int, reversed(str(aux)))
	factors = cycle(range(2,8))
	s = sum(d * f for d, f in zip(revertido,factors))
	res = (-s)%11
 
	if str(res) == dv:
		return True
	elif dv=="K" and res==10:
		return True
	else:
		raise ValidationError(
            'El rut no es valido',
            params={'rut': rut},
        )

def validar_fecha(value):
    if value.year > 2000:
        raise ValidationError(
            'La edad no es valida, debes ser mayor de 18 años',
            params={'value': value},
        )

def validar_letras(value):
    if not re.match(r"[A-ZáÁéÉíÍóÓúÚñÑa-z\s]+$", value):
        raise ValidationError(
            'El nombre no es valido, solo se permiten letras',
            params={'value': value},
        )

