from django.shortcuts import render, redirect
from django.http import HttpResponse
# from django.contrib.auth.models import User
from .models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy
from .forms import RegistroForm
from django.contrib.auth import logout

from apps.rescatados.models import Perro

# Create your views here.

def index(request):   
    perro = Perro.objects.all()
    context = {'perros':perro} 
    return render(request, 'mantenedor/index.html', context)

# def registro(request):      
#     return render(request, 'mantenedor/registro.html')

def passwordReset(request):
    return render(request, 'mantenedor/password_reset.html')

class RegistroUsuario(CreateView):
    model = User
    template_name = "mantenedor/registro.html"
    form_class = RegistroForm
    success_url = reverse_lazy('login')

def logOut(request):
    logout(request)
    return redirect('/')

