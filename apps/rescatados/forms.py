from .models import Perro
from django import forms

class RescatadosForm(forms.ModelForm):
    

    class Meta:
        model = Perro
        fields = (
            'fotografia',
            'nombre',
            'razaPredominante',
            'descripcion',
            'estado'
        )
        labels = {
            'fotografia':'Fotografia',
            'nombre':'Nombre Mascota',
            'razaPredominante':'Raza',
            'descripcion':'Descripcion',
            'estado':'Estado'
        }
        widgets = {
            # 'fotografia':forms.FileInput(attrs={'class':'form-control','type':'file'}),
            'nombre':forms.TextInput(attrs={'class':'form-control'}),
            'razaPredominante':forms.TextInput(attrs={'class':'form-control'}),
            'descripcion':forms.TextInput(attrs={'class':'form-control'}),
            'estado':forms.Select(choices="PERRO_ESTADOS", attrs={'class':'form-control'}),
        }

    
