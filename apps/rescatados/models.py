from django.db import models
# from django.forms import ModelForm

# Create your models here.

PERRO_ESTADOS = (
    ('Disponible', 'Disponible'),
    ('Rescatado', 'Rescatado'),
    ('Adoptado', 'Adoptado'),
)

class Perro (models.Model):

    fotografia = models.ImageField(upload_to='rescatados')
    nombre = models.CharField(max_length=50)
    razaPredominante = models.CharField(max_length=50)
    descripcion = models.TextField(max_length=150)
    estado = models.CharField(max_length=50, choices=PERRO_ESTADOS)

    def __unicode__(self,):
        return str(self.fotografia)

