from django.urls import path
from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from .views import RescatadosList, RescatadosCreate, RescatadosUpdate, RescatadosDelete, RescatadosListAdopcion

app_name = 'rescatados'

urlpatterns = [
    path('listar/', RescatadosList.as_view(), name="rescatados_list"),
    # path('listar/', rescatados_listar, name="rescatados_list"), 
    path('crear/', RescatadosCreate.as_view(), name="rescatados_form"),
    path('editar/<int:pk>', RescatadosUpdate.as_view(), name="rescatados_update"),
    path('borrar/<int:pk>', RescatadosDelete.as_view(), name="rescatados_borrar"),
    path('adopcion', RescatadosListAdopcion.as_view(), name="rescatados_adopcion")
]