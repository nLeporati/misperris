from django.shortcuts import render, redirect
from django.contrib.auth.mixins import UserPassesTestMixin, AccessMixin, LoginRequiredMixin
from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Perro
from .forms import RescatadosForm
# Create your views here.

class UserLoginSuperuser(UserPassesTestMixin):
    login_url = 'login'
    redirect_field_name = 'next'

    def test_func(self):
        return self.request.user.is_superuser
        
    # def handle_no_permission(self):
    #     return redirect('index')


class RescatadosList (UserLoginSuperuser, ListView):                    
    model = Perro
    template_name = 'rescatados/rescatados_list.html'


class RescatadosCreate (UserLoginSuperuser, CreateView):
    model = Perro
    form_class = RescatadosForm
    template_name = 'rescatados/rescatados_form.html'
    success_url = reverse_lazy('rescatados:rescatados_list')


class RescatadosUpdate(UserLoginSuperuser, UpdateView):
    model = Perro
    form_class = RescatadosForm
    template_name = 'rescatados/rescatados_form.html'
    success_url = reverse_lazy('rescatados:rescatados_list')


class RescatadosDelete(UserLoginSuperuser, DeleteView):
    model = Perro
    template_name = 'rescatados/rescatados_borrar.html'
    success_url = reverse_lazy('rescatados:rescatados_list')


class RescatadosListAdopcion(ListView):
    model = Perro
    queryset = Perro.objects.filter(estado="Disponible")
    template_name = 'rescatados/rescatados_adopcion.html'