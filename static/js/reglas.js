var Fn = {
    // Valida el rut con su cadena completa "XXXXXXXX-X"
    validaRut : function (txtRun) {
        txtRun = txtRun.replace("‐","-");
        if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( txtRun ))
            return false;
        var tmp     = txtRun.split('-');
        var digv    = tmp[1]; 
        var rut     = tmp[0];
        if ( digv == 'K' ) digv = 'k' ;
        
        return (Fn.dv(rut) == digv );
    },
    dv : function(T){
        var M=0,S=1;
        for(;T;T=Math.floor(T/10))
            S=(S+T%10*(9-M++%6))%11;
        return S?S-1:'k';
    }
};

// funcion validar solo letras al escribir
function validarLetras(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla == 8)
        return true; 
    patron = /[A-ZáÁéÉíÍóÓúÚñÑa-z\s]+$/i; 
    te = String.fromCharCode(tecla); 
    return patron.test(te);
}

// funcion validar solo numeros al escribir
function validarNumeros(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    patron = /[^\d]+$/; 
    te = String.fromCharCode(tecla); 
    return !patron.test(te); 
}

// funcion validar formato rut al escribir
function validarRut(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    patron = /[^\d\-\k\K]/; 
    te = String.fromCharCode(tecla); 
    return !patron.test(te); 
}

